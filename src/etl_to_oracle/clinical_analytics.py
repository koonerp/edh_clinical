import boto3
from botocore.client import Config
import json
import os

# Get secret key from Secrets Manager
def get_secret(p_secret_id, p_secret_key):
    sm_client = boto3.client('secretsmanager', endpoint_url='https://secretsmanager.' + os.environ['AWS_REGION'] + '.amazonaws.com')
    get_secret_value_response = sm_client.get_secret_value(SecretId=p_secret_id)
    if 'SecretString' in get_secret_value_response:
        secret_string = json.loads(get_secret_value_response['SecretString'])
        secret_value = secret_string[p_secret_key]
        return secret_value
    else:
        print('SecretString not found !!!')
        
def get_dynamodb_item(table_name, service_name):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(table_name)
    resp = table.get_item(Key={'service_name': service_name})
    return resp['Item']