import json
import boto3
import os
import pandas as pd
import cx_Oracle
import time
from datetime import datetime
import numpy as np
from clinical_analytics import get_secret, get_dynamodb_item

def lambda_handler(event, context):
    t1 = time.perf_counter()
    print(f'[{datetime.now()}] Module Execution Started')
    dynamodb_table_name = event['Input']['Arguments']['--dynamodb_table']
    service_name = event['Input']['Arguments']['--service_name']

    # Initialize variables
    etl_conf = get_dynamodb_item(dynamodb_table_name, service_name)
    s3_bucket = etl_conf['s3_bucket']
    s3_output_obj_prefix = etl_conf['s3_output_obj_prefix']
    s3_output_obj_key = etl_conf['s3_output_obj_key']
    db_secret_name = etl_conf['db_secret_name']
    db_batch_size = etl_conf['db_batch_size']
    db_insert_sql = etl_conf['db_insert_sql']
    db_truncate_sql = etl_conf['db_truncate_sql']
    kms_key_id = etl_conf['kms_key_id']

    print(f'[{datetime.now()}] s3_bucket [{s3_bucket}]')
    print(f'[{datetime.now()}] s3_output_obj_prefix [{s3_output_obj_prefix}]')
    print(f'[{datetime.now()}] s3_output_obj_key [{s3_output_obj_key}]')
    print(f'[{datetime.now()}] db_secret_name [{db_secret_name}]')
    print(f'[{datetime.now()}] db_batch_size [{db_batch_size}]')
    print(f'[{datetime.now()}] db_insert_sql [{db_insert_sql}]')
    print(f'[{datetime.now()}] db_truncate_sql [{db_truncate_sql}]')
    print(f'[{datetime.now()}] kms_key_id [{kms_key_id}]')

    s3 = boto3.resource('s3')
    content_object = s3.Object(s3_bucket, (s3_output_obj_prefix + s3_output_obj_key))
    # chunk_size = 400000
    studyDataResultDFChunks = pd.read_csv(content_object.get()['Body'], na_filter=False, chunksize=400000, low_memory=False)
    studyDataResultDF = pd.concat([chunk for chunk in studyDataResultDFChunks])
    # studyDataResultDF = pd.read_csv(content_object.get()['Body'], na_filter=False)
    # studyDataResultDF = studyDataResultDF.applymap(str)
    # studyDataResultDF = studyDataResultDF.replace('nan', "")

    username = get_secret(db_secret_name, 'username')
    password = get_secret(db_secret_name, 'password')
    host = get_secret(db_secret_name, 'host')
    port = get_secret(db_secret_name, 'port')
    dbname = get_secret(db_secret_name, 'dbname')

    print(f'[{datetime.now()}] DB username: [{username}]')
    print(f'[{datetime.now()}] DB password: [{len(password)}]')
    print(f'[{datetime.now()}] DB host: [{host}]')
    print(f'[{datetime.now()}] DB port: [{port}]')
    print(f'[{datetime.now()}] DB SID: [{dbname}]')

    dsn_tns = cx_Oracle.makedsn(host, port, service_name=dbname)

    with cx_Oracle.connect(username, password, dsn_tns, encoding="UTF-8") as conn:
        cursor = conn.cursor()
        cursor.execute(db_truncate_sql)
        print(f'[{datetime.now()}] Deleted all rows from table')

        for index, data in studyDataResultDF.groupby(np.arange(len(studyDataResultDF))//db_batch_size):
            data = data.applymap(str)
            print(data.columns)
            cursor.executemany(db_insert_sql, data.values.tolist())
            print(f'[{datetime.now()}] Inserting into Oracle batch size [{len(data)}]')

        conn.commit()
        cursor.close()
        print(f'[{datetime.now()}] Oracle commit completed')

    print(f'[{datetime.now()}] Module Execution Completed...')
    elapsed = round(time.perf_counter() - t1)
    print(f'Finished in [{elapsed}] seconds')

    response = {}    
    response['message'] = service_name + ' has been completed successfully'
        
    return {
        'statusCode': 200,
        'body': json.dumps(response)
    }
    