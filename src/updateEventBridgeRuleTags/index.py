import json
import boto3
import os
import cfnresponse
import sys

def lambda_handler(event, context):
    ingestionFlag = False

    print(event)
    # print(event['EventBridgeRuleName'])
    # print(event['ApplicationName'])
    responseData = {}
    
    try:
      eventBridgeRuleNames = event['ResourceProperties']['EventBridgeRuleNames']
      applicationName = event['ResourceProperties']['ApplicationName']
      # eventBridgeRuleName = 'edh_clinical_studydataloader_rule'
      # applicationName = 'clinical'

      # Get Environment from Parameter Store
      ssm = boto3.client('ssm')
      parameter = ssm.get_parameter(Name='edh_platform_environment')
      
      environment = None        
      environment = parameter['Parameter']['Value']

      # Get tenant tags from Parameter Store
      ssm = boto3.client('ssm')
      applicationTagsList = ['edh', 'tenant', applicationName, 'tags']      
      tenant_tags = ssm.get_parameter(Name="_".join(applicationTagsList))
      tenant_tags_list = tenant_tags['Parameter']['Value'].split(',')

      # Get Account ID
      sts = boto3.client('sts')
      accountId = sts.get_caller_identity()['Account']

      # Get current region
      runtime_region = os.environ['AWS_REGION']
      print('eventBridgeRuleName : ', eventBridgeRuleNames)

      for eventBridgeRuleName in eventBridgeRuleNames.split(','):
        if eventBridgeRuleName == '' or eventBridgeRuleName == None:
          continue
        
        try:
          resourceArnList = ['arn:aws:events:', runtime_region, ':', str(accountId), ':rule/', eventBridgeRuleName]
          resourceArn = ''.join(resourceArnList)
          print('resourceArn : ', resourceArn)
          events = boto3.client('events')
          print('Before apply tenant tags : ', events.list_tags_for_resource(ResourceARN=resourceArn))
          
          events.tag_resource(ResourceARN=resourceArn, 
                      Tags=[{'Key':'application', 'Value': tenant_tags_list[0]},{'Key': 'costcenter', 'Value': tenant_tags_list[1]},{'Key':'owner', 'Value' : tenant_tags_list[2]},{'Key': 'environment', 'Value': environment}])
                      
          print('After apply tenant tags : ', events.list_tags_for_resource(ResourceARN=resourceArn))
          responseData['Result'] = 'Applied resource tags successfully'
        except:
          exc_type, exc_value, exc_traceback = sys.exc_info()
          print(exc_value)
          print(exc_type)
          print(exc_traceback)

      ingestionFlag = True
    except:
      exc_type, exc_value, exc_traceback = sys.exc_info()
      print(exc_value)
      print(exc_type)
      print(exc_traceback)
      responseData['Result'] = 'Apply resource tags got failed'
      raise
      ingestionFlag = False
    finally:
      print('Process Completed')
      if ingestionFlag:
        cfnresponse.send(event, context, cfnresponse.SUCCESS, responseData, "CustomResourcePhysicalID")
      else:
        cfnresponse.send(event, context, cfnresponse.FAILED , responseData, "CustomResourcePhysicalID")

    return {
        'statusCode': 200,
        'body': json.dumps('Process completed...')
    }
