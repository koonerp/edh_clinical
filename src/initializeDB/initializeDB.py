import json
import boto3

def lambda_handler(event, context):
    print("DynamoDB Ingestion started....")
    ingestionFlag = False

    try:
        # print(event)
        print(event['DynamoTableName'])
        print(event['KMSKey'])
        print(event['S3Bucket'])
        print(event['S3Key'])

        dynamoDBTable = event['DynamoTableName']
        kmsKeyParameter = event['KMSKey']
        S3Bucket = event['S3Bucket']
        S3Key = event['S3Key']

        # Get KMS key from Parameter Store
        ssm = boto3.client('ssm')
        parameter = ssm.get_parameter(Name=kmsKeyParameter)
        
        kmsKey = None        
        paramValue = parameter['Parameter']['Value'].split('/')
        if len(paramValue) == 2:
            kmsKey = paramValue[1]

        parameter = ssm.get_parameter(Name='edh_platform_environment')
        environment = parameter['Parameter']['Value']
        S3Bucket = S3Bucket + '.' + environment
        S3Key = S3Key + '_' + environment + '.json'
        
        # Read the application configuration file from S3 location
        s3 = boto3.resource('s3')
        dynamodb = boto3.resource('dynamodb')

        content_object = s3.Object(S3Bucket, S3Key)
        
        if content_object is not None:
            file_content = content_object.get()['Body'].read()
            json_content = json.loads(file_content)
        
            # Load the JSON data into DynamoDB table
            table = dynamodb.Table(dynamoDBTable)
            for param in json_content:
                param['kms_key_id'] = kmsKey
                print(param)
                table.put_item(Item=param)
            print("Clinical RM Config data ingested into DynamoDB table successfully")
        else:
            print("Clinical RM Config data ingested into DynamoDB table failed. JSON file is not found")
    except:
        raise
    finally:
        print("Process Completed")

    return {
        'statusCode': 200,
        'body': json.dumps('DynamoDB Insert completed successfully!')
    }