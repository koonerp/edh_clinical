import time
from datetime import datetime
import sys
from clinical_analytics import get_dynamodb_item, get_s3_obj, upload_to_s3
import csv
import os
import boto3
import botocore
from awsglue.utils import getResolvedOptions


# ==============================================================================
# Script Begins
# ==============================================================================
t1 = time.perf_counter()
print(f'[{datetime.now()}] Module Execution Started...')

# Initialize SNS object for missing study id's notification
sns = boto3.client('sns')
missingStudyData = list()

# Get job parameters
args = getResolvedOptions(sys.argv, ['dynamodb_table', 'service_name'])
dynamodb_table_name = args['dynamodb_table']
service_name = args['service_name']

print(f'[{datetime.now()}] Parameter: DynamoDB Table Name [{dynamodb_table_name}]')
print(f'[{datetime.now()}] Parameter: Service Name [{service_name}]')

# Initialize variables
etl_conf = get_dynamodb_item(dynamodb_table_name, service_name)
s3_bucket = etl_conf['s3_bucket']
s3_mapping_obj_prefix = etl_conf['s3_mapping_obj_prefix']
s3_mapping_obj_key = etl_conf['s3_mapping_obj_key']
s3_input_obj_prefix = etl_conf['s3_input_obj_prefix']
s3_output_obj_prefix = etl_conf['s3_output_obj_prefix']
s3_output_obj_key = etl_conf['s3_output_obj_key']
kms_key_id = etl_conf['kms_key_id']
formoid_local_dir = os.path.join(os.getcwd(), 's3_files')
if not os.path.exists(formoid_local_dir):
    os.makedirs(formoid_local_dir)

print(f'[{datetime.now()}] s3_bucket [{s3_bucket}]')
print(f'[{datetime.now()}] s3_mapping_obj_prefix [{s3_mapping_obj_prefix}]')
print(f'[{datetime.now()}] s3_mapping_obj_key [{s3_mapping_obj_key}]')
print(f'[{datetime.now()}] s3_input_obj_prefix [{s3_input_obj_prefix}]')
print(f'[{datetime.now()}] s3_output_obj_prefix [{s3_output_obj_prefix}]')
print(f'[{datetime.now()}] s3_output_obj_key [{s3_output_obj_key}]')
print(f'[{datetime.now()}] formoid_local_dir [{formoid_local_dir}]')

# Download mapping object
mapping_filename = s3_mapping_obj_key
with open(mapping_filename, 'wb') as mapping_file:
    get_s3_obj(s3_bucket, s3_mapping_obj_prefix + s3_mapping_obj_key, mapping_file)

print(f'[{datetime.now()}] mapping file [{s3_mapping_obj_prefix + s3_mapping_obj_key}] downloaded')

# Read mapping file
mapping_list = list()
with open(mapping_filename, 'r', newline='', encoding='utf-8-sig') as mapping_file:
    mapping_file_csvreader = csv.DictReader(mapping_file)
    for mapping_row in mapping_file_csvreader:
        row_dict = dict()
        for key, value in mapping_row.items():
            row_dict[key] = value
        mapping_list.append(row_dict)

# Form output file header from mapping
output_file_header = [key for key in mapping_list[0].keys() if key not in ('STUDY_NAME', 'FORM_OID')]

# Open output view file in write mode
output_filename = s3_output_obj_key
with open(output_filename, 'w', newline='', encoding='utf-8') as output_file:
    output_file_csvwriter = csv.DictWriter(output_file, fieldnames=output_file_header, delimiter=',')
    # Write output file header
    output_file_csvwriter.writeheader()

    # Iterate over all mapping rows
    for mapping_row_item in mapping_list:
        # Defensive code replacing space in study name with underscore (in case Greg's team missed any)
        study_name = mapping_row_item['STUDY_NAME'].strip().replace(' ', '_')
        formoid = mapping_row_item['FORM_OID'].strip() + '.csv'
        formoid_s3_obj = s3_input_obj_prefix + study_name + '/' + formoid
        formoid_local_file = os.path.join(formoid_local_dir, formoid)
        print('formoid_local_file : ', formoid_local_file)
        try:
            with open(formoid_local_file, 'wb') as input_data:
                get_s3_obj(s3_bucket, formoid_s3_obj, input_data)
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                # TODO: Need to log this in a table
                print('[ERROR]', formoid_s3_obj, 'is missing, please take action!')
                missingStudyData.append(formoid_s3_obj + ' is missing, please take action!')
            elif e.response['Error']['Code'] == "403":
                print('[ERROR]', formoid_s3_obj, 'is missing, please take action!')
                missingStudyData.append(formoid_s3_obj + ' is missing, please take action!')
            else:
                raise
        else:
            print(f'[{datetime.now()}] [{formoid_s3_obj}] downloaded to [{formoid_local_file}]')
            # Open FormOID file in read mode
            with open(formoid_local_file, 'r', newline='', encoding='utf-8') as formoid_csv:
                formoid_csvreader = csv.DictReader(formoid_csv)
                # Skipping the header row in FormOID file
                next(formoid_csvreader)
                output_row = dict()
                # Read all rows from FormOID csv file
                for formoid_row in formoid_csvreader:
                    if not formoid_row['userid'] == 'EOF':
                        for key, value in mapping_row_item.items():
                            key = key.strip()
                            value = value.strip()
                            if value == '<NA>':
                                value = ''
                            if key not in ('STUDY_NAME', 'FORM_OID'):
                                output_row[key] = formoid_row.get(value, value)
                        output_file_csvwriter.writerow(output_row)
            print(f'[{datetime.now()}] processing completed for [{formoid_s3_obj}]')

print(f'[{datetime.now()}] [{output_filename}] file write completed')
upload_to_s3(output_filename, s3_bucket, s3_output_obj_prefix, s3_output_obj_key, kms_key_id)
print(f'[{datetime.now()}] [{output_filename}] uploaded to S3 [{s3_output_obj_prefix}]')

sts = boto3.client("sts")
user_account = sts.get_caller_identity()["Account"]

currentSession = boto3.session.Session()
currentRegion = currentSession.region_name
# missingStudyData.append('Testing.... is missing, please take action!')
print('sns topic arn :', ('arn:aws:sns:' + currentRegion + ':' + user_account + ':edh_clinical_sns_app_users'), ':')
print('no.of missing study data : ', len(missingStudyData))
if len(missingStudyData) > 0:
    missingStudyDataStr = '\r\n'.join(missingStudyData)
    response = sns.publish(
        TopicArn='arn:aws:sns:' + currentRegion + ':' + user_account + ':edh_clinical_sns_app_users',
        # TopicArn='arn:aws:sns:us-east-1:133773883798:edh_clinical_sns_etl',
        Subject='Missing Studies or Form OIDs - ' + service_name,
        Message='Hello,\r\n\r\nThe following studies or form OIDs are missing from the mapping file.\r\n\r\n' + missingStudyDataStr
        )
print(f'[{datetime.now()}] Module Execution Completed...')
t2 = round(time.perf_counter() - t1)
print(f'Finished in [{t2}] seconds')
# ====================================================================
# Script Ends
# ====================================================================
