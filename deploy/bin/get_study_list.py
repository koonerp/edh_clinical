import time
import sys
from awsglue.utils import getResolvedOptions
from datetime import datetime
from clinical_analytics import get_dynamodb_item, get_secret, call_rave_service, write_to_local_file, upload_to_s3


# ====================================================================
# Script Begins
# ====================================================================
t1 = time.perf_counter()
print(f'[{datetime.now()}] Module Execution Started...')

args = getResolvedOptions(sys.argv, ['dynamodb_table', 'service_name'])
dynamodb_table_name = args['dynamodb_table']
service_name = args['service_name']

print(f'[{datetime.now()}] Parameter: DynamoDB Table Name [{dynamodb_table_name}]')
print(f'[{datetime.now()}] Parameter: Service Name [{service_name}]')

item = get_dynamodb_item(dynamodb_table_name, service_name)
secret_name = item['secret_id']
kms_key_id = item['kms_key_id']
bucket_name = item['s3_bucket']
object_prefix = item['s3_object_prefix']
object_key = item['s3_object_key'].replace('$$timestamp$$', datetime.now().strftime("%Y%m%d_%H%M%S.%f"))
uri = item['uri_endpoint'] + item['uri_resource']


# Get Rave API user credentials from AWS Secrets Manager
print(f'[{datetime.now()}] Getting Rave user credentials from AWS Secrets Manager for [{secret_name}]')
secret_username = get_secret(secret_name, 'username')
secret_password = get_secret(secret_name, 'password')
print(f'[{datetime.now()}] Secret username [{secret_username}], Password length[{len(secret_password)}]')

# Call Studies List Service
print(f'[{datetime.now()}] URI [{uri}]')

with call_rave_service(uri, secret_username, secret_password) as resp:
    write_to_local_file(object_key, resp.content)
    print(f'[{datetime.now()}] Local file write completed')
    upload_to_s3(object_key, bucket_name, object_prefix, object_key, kms_key_id)
    print(f'[{datetime.now()}] Uploaded to S3')

print(f'[{datetime.now()}] Module Execution Completed...')
t2 = round(time.perf_counter() - t1)
print(f'Finished in [{t2}] seconds')
# ====================================================================
# Script Ends
# ====================================================================
