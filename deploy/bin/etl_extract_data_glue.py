import sys
import pandas as pd
import boto3
import botocore
from botocore.client import Config
import time
from datetime import datetime
from awsglue.utils import getResolvedOptions
import pyarrow.parquet as pq
import s3fs
import concurrent.futures
import os
from io import StringIO 
from clinical_analytics import get_dynamodb_item, sendNotification, upload_to_s3, get_DataFrame_From_CSV, getDataFrame_From_Parquet, convertBigIntToDateTime, get_DataFrame_From_SAS

# ==============================================================================
# Script Begins
# ==============================================================================

t1 = time.perf_counter()
print(f'[{datetime.now()}] Module Execution Started')

# Initialize SNS object for missing study id's notification
# sns = boto3.client('sns')

missingStudyData = list()
outputDFList = []
metadataConfig = {}

# Get job parameters
args = getResolvedOptions(sys.argv, ['dynamodb_table', 'service_name'])
dynamodb_table_name = args['dynamodb_table']
service_name = args['service_name']

# dynamodb_table_name = 'edh_clinical_config'
# service_name = 'etl_device'

print(f'[{datetime.now()}] Parameter: DynamoDB Table Name [{dynamodb_table_name}]')
print(f'[{datetime.now()}] Parameter: Service Name [{service_name}]')

# Initialize variables
etl_conf = get_dynamodb_item(dynamodb_table_name, service_name)
s3_bucket = etl_conf['s3_bucket']
s3_mapping_obj_prefix = etl_conf['s3_mapping_obj_prefix']
s3_mapping_obj_key = etl_conf['s3_mapping_obj_key']
s3_output_obj_prefix = etl_conf['s3_output_obj_prefix']
s3_output_obj_key = etl_conf['s3_output_obj_key']
kms_key_id = etl_conf['kms_key_id']

print(f'[{datetime.now()}] s3_bucket [{s3_bucket}]')
print(f'[{datetime.now()}] s3_mapping_obj_prefix [{s3_mapping_obj_prefix}]')
print(f'[{datetime.now()}] s3_mapping_obj_key [{s3_mapping_obj_key}]')
print(f'[{datetime.now()}] s3_output_obj_prefix [{s3_output_obj_prefix}]')
print(f'[{datetime.now()}] s3_output_obj_key [{s3_output_obj_key}]')

s3 = boto3.resource('s3')
content_object = s3.Object(s3_bucket, (s3_mapping_obj_prefix + s3_mapping_obj_key))
mappingContent=content_object.get()['Body'].read().decode('UTF-8-SIG')
# mappingDF = pd.read_csv(content_object.get()['Body'], sep= encoding = "ansi")
mappingDF = pd.read_csv(StringIO(mappingContent))
mappingDF = mappingDF.applymap(str)
mappingDF = mappingDF.replace('<NA>', "")
mappingDF = mappingDF.replace('nan', "")

columnNames = list(mappingDF.columns)
columnNames.remove('STUDY_NAME')
columnNames.remove('FORM_OID')
print(columnNames)
print('No.of rows from mapping file : ', len(mappingDF))
inputList = []
for i in range(len(mappingDF)):
    sourceID = mappingDF.loc[i, 'SOURCEID']

    if len(sourceID) > 0:
        if sourceID.startswith('"') and sourceID.endswith('"'):
            sourceID = sourceID[1:-1]

        if sourceID not in metadataConfig:
            metadataConfig[sourceID] = get_dynamodb_item(dynamodb_table_name, sourceID)

        inputList.append({'row': mappingDF.loc[i], 'metadataConfig': metadataConfig[sourceID]})

def extractData(inputDict):
    inputFileKey=''
    inputFormat=''
    sourceMetadataConfig = inputDict.get('metadataConfig')
    # print(inputDict.get('row')['STUDY_NAME'])
    
    inputFormat = sourceMetadataConfig.get('format')
    inputS3Bucket = sourceMetadataConfig.get('s3_bucket')
    result = tuple()
    sourceName=''
    if inputFormat == 'csv':
        inputFileKey = sourceMetadataConfig.get('s3_object_prefix') + sourceMetadataConfig.get('s3_object_key')
        inputFileKey = inputFileKey.replace('$$study_id$$',inputDict.get('row')['STUDY_NAME']).replace('$$formoid$$', inputDict.get('row')['FORM_OID'])
        sourceName=inputFileKey
        result = get_DataFrame_From_CSV(inputS3Bucket, inputFileKey)

        if result[0] == 'FILE-NOT-FOUND':
            print('[ERROR]', inputFileKey, ' is missing, Please take action!')
            missingStudyData.append(inputFileKey + ' is missing, Please take action!')
            return 'MISSING', inputFileKey + ' is missing, Please take action!'
        elif result[0] == 'FORMAT-ERROR':
            print('[ERROR]', inputFileKey, ' is not in CSV format, Please take action!')
            missingStudyData.append(inputFileKey + ', is not in CSV format, Please take action!')
            return 'FORMAT-ERROR', inputFileKey + ' is not in CSV format, Please take action!'
    elif inputFormat == 'sas':
        inputFileKey = sourceMetadataConfig.get('s3_object_prefix') + sourceMetadataConfig.get('s3_object_key')
        inputFileKey = inputFileKey.replace('$$study_id$$',inputDict.get('row')['STUDY_NAME']).replace('$$formoid$$', inputDict.get('row')['FORM_OID'])
        sourceName=inputFileKey
        result = get_DataFrame_From_SAS(inputS3Bucket, inputFileKey)

        if result[0] == 'FILE-NOT-FOUND':
            print('[ERROR]', inputFileKey, ' is missing, Please take action!')
            missingStudyData.append(inputFileKey + ' is missing, Please take action!')
            return 'MISSING', inputFileKey + ' is missing, Please take action!'
        elif result[0] == 'FORMAT-ERROR':
            print('[ERROR]', inputFileKey, ' is not in SAS7BDAT format, Please take action!')
            missingStudyData.append(inputFileKey + ', is not in SAS7BDAT format, Please take action!')
            return 'FORMAT-ERROR', inputFileKey + ' is not in SAS7BDAT format, Please take action!'

    elif inputFormat == 'parquet':
        inputFileKey = sourceMetadataConfig.get('s3_object_prefix')
        table_name = inputDict.get('row')['FORM_OID']
        inputFileKey = inputFileKey.replace('$$table_name$$', table_name.lower())
        
        result = getDataFrame_From_Parquet(inputS3Bucket, inputFileKey)
        sourceName=sourceMetadataConfig.get('service_name') + ' - ' + table_name
        
        print('[INFO]', sourceName, ' - Get data - service _name : ', sourceMetadataConfig.get('service_name'))
        if result[0] == 'FILE-NOT-FOUND':
            print('[ERROR]', sourceName, ' is missing, Please take action!')
            missingStudyData.append(sourceName + ' is missing, Please take action!')
            return 'MISSING', sourceName + ' is missing, Please take action!'
        elif result[0] == 0:
            print('[ERROR] ', sourceName, ' does not have data. Please take action!')
            missingStudyData.append(sourceName , ' does not have data. Please take action!')
            return 'MISSING', sourceName, ' does not have data. Please take action!'
    else:
        print(f'[{datetime.now()}] Unsupported Input Format [{inputFormat}]')
        return 'ERROR', 'Unsupported Input Format ' + inputFormat

    inputFileDF = result[1]

    # inputFileColumns = list()
    missingColumns={}
    invalidColumns=[]
    resultDF = pd.DataFrame()
    for column in columnNames:
        # print(column, ' : ', inputDict.get('row')[column])
        columnNameFromMapping = inputDict.get('row')[column].strip()
        if columnNameFromMapping in inputFileDF:
            # print('-----Present as is ', columnNameFromMapping)
            try:
                resultDF[column]=inputFileDF[columnNameFromMapping]
            except TypeError as error:
                print('Type Error in : ', inputFileKey )
                raise error
        elif columnNameFromMapping.lower() in inputFileDF:
            # print('-----Present in lower case ', columnNameFromMapping)
            resultDF[column]=inputFileDF[columnNameFromMapping.lower()]
        else:
            # print('-----Not Present ',column, ':', columnNameFromMapping)
            if columnNameFromMapping.startswith('"') and columnNameFromMapping.endswith('"'):
                # print('constant : ', columnNameFromMapping[1:-1])
                missingColumns[column] = columnNameFromMapping[1:-1]
            else:
                if len(columnNameFromMapping) > 0:
                    invalidColumns.append(columnNameFromMapping)

                missingColumns[column] = ''
                # print('Column not found in the source')

    if len(invalidColumns) > 0 :
        missingStudyData.append(sourceName + " - " + ", ".join(invalidColumns) + " column(s) is/are missing")
    
    inputFileDF = None
    for key, value in missingColumns.items():
        resultDF[key] = value
    
    # # print(resultDF.columns)
    # tempDF = resultDF[(resultDF.STUDYID == ' be viewed by browsers running on the local server machine.') | (resultDF.STUDYID == ' Geneva') | (resultDF.STUDYID == 'Monospace;font-size:11pt;margin:0;padding:0.5em;line-height:14pt}')]
    # if len(tempDF) > 0:
    #     print('Geneva data from : ', inputFileKey)
    # print(resultDF[columnNames].head())
    return ('SUCCESS-' + sourceName), resultDF[columnNames] #pd.DataFrame(df, columns = columnNames)

with concurrent.futures.ThreadPoolExecutor(50) as extractor_executor:
    extract_results = {extractor_executor.submit(extractData, inputDict): inputDict for inputDict in inputList}
    for extract_result in concurrent.futures.as_completed(extract_results):
        try:
            # if (extract_result.result()[0] == 'SUCCESS'):
            if (extract_result.result()[0].startswith('SUCCESS')):
                outputDFList.append(extract_result.result()[1])
        except Exception as e:
            # print(e)
            raise e

output_filename = s3_output_obj_key
print('Output file : ', output_filename)
with open(output_filename, 'a', encoding='UTF-8-SIG') as output_file:
    headerFlag = True
    for df in outputDFList:
        if headerFlag:
            df.to_csv(output_file, index=False)
            headerFlag = False
        else:
            df.to_csv(output_file, header=None,index=False)

print(f'[{datetime.now()}] [{output_filename}] file write completed')
upload_to_s3(output_filename, s3_bucket, s3_output_obj_prefix, s3_output_obj_key, kms_key_id)
print(f'[{datetime.now()}] [{output_filename}] uploaded to S3 [{s3_output_obj_prefix}]')

os.remove(output_filename)

print('No.of missing studies : ', len(missingStudyData))
missingStudyData = list(dict.fromkeys(missingStudyData))
print('No.of missing studies after removing duplicate file names : ', len(missingStudyData))
if len(missingStudyData) > 0:
    ssm = boto3.client('ssm')
    parameterStore = ssm.get_parameter(Name='edh_platform_environment')
    
    environment = parameterStore['Parameter']['Value']
    
    missingStudyDataStr = ".\n".join(missingStudyData)
    missingStudyDataStr=missingStudyDataStr+"."
    subject = '''[''' + environment + '''] Missing Studies or Form OIDs - ''' + service_name
    message = 'Hello,\r\n\r\nThe following studies or form OIDs are missing from the mapping file.\r\n\r\n' + missingStudyDataStr
    print(message)
    sendNotification('edh_clinical_sns_app_users', subject, message)

print(f'[{datetime.now()}] Module Execution Completed...')
elapsed = round(time.perf_counter() - t1)
print(f'Finished in [{elapsed}] seconds')
# ====================================================================
# Script Ends
# ====================================================================
