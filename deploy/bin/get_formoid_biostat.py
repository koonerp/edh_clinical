import sys
import time
import os
from datetime import datetime
from awsglue.utils import getResolvedOptions
from clinical_analytics import get_dynamodb_item, get_secret, call_rave_service, get_study_oids_prod_suffix, \
    write_to_local_file, upload_to_s3
import concurrent.futures
import xml.etree.ElementTree as ET


# ====================================================================
# Script Begins
# ====================================================================
t1 = time.perf_counter()
print(f'[{datetime.now()}] Module Execution Started...')

args = getResolvedOptions(sys.argv, ['dynamodb_table', 'service_name'])
dynamodb_table_name = args['dynamodb_table']
service_name = args['service_name']

print(f'[{datetime.now()}] Parameter: DynamoDB Table Name [{dynamodb_table_name}]')
print(f'[{datetime.now()}] Parameter: Service Name [{service_name}]')

biostat_conf = get_dynamodb_item(dynamodb_table_name, service_name)
secret_name = biostat_conf['secret_id']
kms_key_id = biostat_conf['kms_key_id']
bucket_name = biostat_conf['s3_bucket']
studylist_service_name = biostat_conf['study_list_service_name']
metadata_service_name = biostat_conf['study_metadata_service_name']

# Get Rave API user credentials from AWS Secrets Manager
print(f'[{datetime.now()}] Getting Rave user credentials from AWS Secrets Manager for [{secret_name}]')
secret_username = get_secret(secret_name, 'username')
secret_password = get_secret(secret_name, 'password')
print(f'[{datetime.now()}] Secret username [{secret_username}], Password length [{len(secret_password)}]')

# Get Study list
t1_studylist = time.perf_counter()
studylist_conf = get_dynamodb_item(dynamodb_table_name, studylist_service_name)
studylist_obj_prefix = studylist_conf['s3_object_prefix']
studylist_obj_key = studylist_conf['s3_object_key'].replace('$$timestamp$$', datetime.now().strftime("%Y%m%d_%H%M%S.%f"))
studylist_dir_path = os.path.join(os.getcwd(), studylist_obj_prefix)
if not os.path.exists(studylist_dir_path):
    os.makedirs(studylist_dir_path)
studylist_localfile = os.path.join(studylist_dir_path, studylist_obj_key)

studylist_uri = studylist_conf['uri_endpoint'] + studylist_conf['uri_resource']
print(f'[{datetime.now()}] Study List URI [{studylist_uri}]')
with call_rave_service(studylist_uri, secret_username, secret_password) as resp:
    print(f'[{datetime.now()}] Study List local file [{studylist_localfile}]')
    write_to_local_file(studylist_localfile, resp.content)
    print(f'[{datetime.now()}] Study List local file write completed')
    upload_to_s3(studylist_localfile, bucket_name, studylist_obj_prefix, studylist_obj_key, kms_key_id)
    print(f'[{datetime.now()}] Study List uploaded to S3')

with open(studylist_localfile, 'rb') as f:
    # all_studyoids = get_study_oids(f.name)
    # just in Dev, call above function in PROD
    all_studyoids = get_study_oids_prod_suffix(f.name)
print(f'[{datetime.now()}] All (Prod) suffix Study OIDs [{len(all_studyoids)}] {all_studyoids}')

studyoids = []
if 'exclusion_list' in studylist_conf:
    exclusion_list = list(studylist_conf['exclusion_list'].split(','))
    studyoids = [study for study in all_studyoids if study not in exclusion_list]
else:
    studyoids = all_studyoids
    
print(f'[{datetime.now()}] Study OIDs after exclusion [{len(studyoids)}] {studyoids}')
t2_studylist = round(time.perf_counter() - t1_studylist)
print(f'[{datetime.now()}] Study List processing finished in [{t2_studylist}] seconds')


def download_biostat_data(study_name):
    t1_biostat = time.perf_counter()
    debug_msgs = []
    study_name_for_s3 = study_name.replace(' ', '_')
    study_name_for_uri = study_name.replace(' ', '%20')

    # Get Study Metadata
    t1_metadata = time.perf_counter()
    metadata_conf = get_dynamodb_item(dynamodb_table_name, metadata_service_name)
    metadata_obj_prefix = metadata_conf['s3_object_prefix']
    metadata_dir_path = os.path.join(os.getcwd(), metadata_obj_prefix, study_name_for_s3)
    if not os.path.exists(metadata_dir_path):
        os.makedirs(metadata_dir_path)
    metadata_obj_key = metadata_conf['s3_object_key'].replace('$$study_id$$', study_name_for_s3).replace('$$timestamp$$', datetime.now().strftime("%Y%m%d_%H%M%S.%f"))
    metadata_localfile = os.path.join(metadata_dir_path, metadata_obj_key)
    metadata_uri = metadata_conf['uri_endpoint']
    metadata_uri += metadata_conf['uri_resource'].replace('$$study_id$$', study_name_for_uri)
    if 'uri_parameters' in metadata_conf:
        metadata_uri += metadata_conf['uri_parameters'].replace('$$study_id_param$$', study_name)

    debug_msgs.append(f'[{datetime.now()}] [{study_name}] Metadata URI [{metadata_uri}]')
    formoids = []

    downloadFlag = False
    retry = 0
    while not downloadFlag:
        tree = None
        root = None
        try:        
            with call_rave_service(metadata_uri, secret_username, secret_password) as meta_resp:
                debug_msgs.append(f'[{datetime.now()}] [{study_name}] Metadata local file [{metadata_localfile}]')
                write_to_local_file(metadata_localfile, meta_resp.content)
                debug_msgs.append(f'[{datetime.now()}] [{study_name}] Metadata local file write completed')
                upload_to_s3(metadata_localfile, bucket_name, metadata_obj_prefix, metadata_obj_key, kms_key_id)
                debug_msgs.append(f'[{datetime.now()}] [{study_name}] Metadata uploaded to S3')

        
            tree = ET.parse(metadata_localfile)
            root = tree.getroot()
            formoids = [elem.attrib['OID'] for elem in root[0][1] if 'FormDef' in elem.tag]
            downloadFlag = True
        except ET.ParseError as parserError:
            print(parserError)
            retry += 1
            if retry == 5:
                downloadFlag = True
        except IndexError as indexError:
            print("Index Error : ")
            print(tree)
            print("============================= root ===============================")
            print(root)
            print(indexError)
            retry += 1
            if retry == 5:
                downloadFlag = True

    debug_msgs.append(f'[{datetime.now()}] [{study_name}] FormOIDs [{len(formoids)}] {formoids}')
    metadata_exec = round(time.perf_counter() - t1_metadata)
    debug_msgs.append(f'[{datetime.now()}] [{study_name}] Metadata finished in [{metadata_exec}] seconds')

    biostat_obj_prefix = biostat_conf['s3_object_prefix'].replace('$$study_id$$', study_name_for_s3)
    biostat_dir_path = os.path.join(os.getcwd(), biostat_obj_prefix)
    if not os.path.exists(biostat_dir_path):
        os.makedirs(biostat_dir_path)

    def process_formoids(formoid):
        biostat_obj_key = biostat_conf['s3_object_key'].replace('$$formoid$$', formoid)
        biostat_localfile = os.path.join(biostat_dir_path, biostat_obj_key)

        biostat_uri = biostat_conf['uri_endpoint']
        biostat_uri += biostat_conf['uri_resource'].replace('$$study_id$$', study_name_for_uri)
        biostat_uri = biostat_uri.replace('$$formoid$$', formoid)

        debug_msgs.append(f'[{datetime.now()}] [{study_name}] [{formoid}] URI [{biostat_uri}]')
        downloadFlag = False
        retry = 0
        while not downloadFlag:
            with call_rave_service(biostat_uri, secret_username, secret_password) as formoid_resp:
                # Try try logic when there is a File not Found/Server Error/RunTime error
                if formoid_resp.status_code == 404 or '<title>Runtime Error</title>' in formoid_resp.content.decode():
                    retry += 1
                    debug_msgs.append(f'[{datetime.now()}] [{study_name}] [{formoid}] is having runtime error/Not Found. Response Code : [{formoid_resp.status_code}]. Try Again to download - [{retry}]')
                    if retry == 5:
                        downloadFlag = True
                    continue                    
                elif formoid_resp.status_code != 200 or len(formoid_resp.content) == 0:
                    debug_msgs.append(f'Empty File : [{biostat_localfile}] : Resonse Code : [{formoid_resp.status_code}] : Content Size : [{len(formoid_resp.content)}] : [{(formoid_resp.status_code != 200)}]')
                    retry += 1
                    if retry == 5:
                        downloadFlag = True
                    continue
                
                debug_msgs.append(f'[{datetime.now()}] [{study_name}] [{formoid}] Wilson Test : [{(formoid_resp.status_code == 404)}] : [{("<title>Runtime Error</title>" in formoid_resp.content.decode())}] : [{(len(formoid_resp.content) == 0)}]')
                debug_msgs.append(f'[{datetime.now()}] [{study_name}] [{formoid}] local file [{biostat_localfile}]')
                write_to_local_file(biostat_localfile, formoid_resp.content)
                debug_msgs.append(f'[{datetime.now()}] [{study_name}] [{formoid}] local file write completed. File size : [{(os.path.getsize(biostat_localfile))}] : Content Size : [{len(formoid_resp.content)}] : Response Status : [{formoid_resp.status_code}]')
                upload_to_s3(biostat_localfile, bucket_name, biostat_obj_prefix, biostat_obj_key, kms_key_id)
                debug_msgs.append(f'[{datetime.now()}] [{study_name}] [{formoid}] uploaded to S3 : Resonse Code : [{formoid_resp.status_code}] : Content Size : [{len(formoid_resp.content)}]')
                downloadFlag = True

        return f'[{datetime.now()}] [{formoid} processing completed]'

    with concurrent.futures.ThreadPoolExecutor() as formoid_executor:
        formoid_results = {formoid_executor.submit(process_formoids, form_oid): form_oid for form_oid in formoids}
        for formoid_result in concurrent.futures.as_completed(formoid_results):
            try:
                print(formoid_result.result())
            except Exception as e:
                print(e)
                raise e

    t2_biostat = round(time.perf_counter() - t1_biostat)
    debug_msgs.append(f'[{datetime.now()}] [{study_name}] Finished in [{t2_biostat}] seconds')

    return debug_msgs


with concurrent.futures.ThreadPoolExecutor(max_workers=50) as executor:
    study_results = {executor.submit(download_biostat_data, study_oid): study_oid for study_oid in studyoids}
    for study_result in concurrent.futures.as_completed(study_results):
        for study_ret_msg in study_result.result():
            try:
                print(study_ret_msg)
            except Exception as ex:
                print(ex)
                raise ex

print(f'[{datetime.now()}] Module Execution Completed...')
t2 = round(time.perf_counter() - t1)
print(f'Finished in [{t2}] seconds')
# ====================================================================
# Script Ends
# ====================================================================
