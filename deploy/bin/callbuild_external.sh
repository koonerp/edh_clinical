#!/bin/bash

echo 'Starting the Ingestion...'

# db=$1
env=$1
alltable=$2
# target=$4
stagingdb=$3
targetdb=$4
view=$5
secrets=$6
bucket=$7
connection_string=${8}

#echo 'db:'$db
#echo 'env:'$env
#echo 'alltable:'$alltable
#echo 'target:'$target
#echo 'stagingdb:'$stagingdb
#echo 'targetdb:'$targetdb
#echo 'view:'$view
#echo 'secrets:'$secrets
#echo 'bucket:'$bucket

#applicationBucket="s3://"${bucket}"/deploy/bin/"
frameworkBucket="s3://edh.platform."${env}
#echo 'Application Location from callbuild_external.sh:'${applicationBucket}

aws s3 cp ${frameworkBucket}/frameworks/general/sqoop_build.sh .

emrfs import s3://${bucket}/data/stage/edh_clinical_crmprd_staging

emrfs import s3://${bucket}/data/stage/edh_clinical_intprd_staging

emrfs import s3://${bucket}/data/target/edh_clinical_crmprd_target

emrfs import s3://${bucket}/data/target/edh_clinical_intprd_target

emrfs import s3://${bucket}/data/views/edh_clinical_crmprd

emrfs import s3://${bucket}/data/views/edh_clinical_intprd

emrfs sync s3://${bucket}/data/stage/edh_clinical_crmprd_staging

emrfs sync s3://${bucket}/data/stage/edh_clinical_intprd_staging

emrfs sync s3://${bucket}/data/target/edh_clinical_crmprd_target

emrfs sync s3://${bucket}/data/target/edh_clinical_intprd_target

emrfs sync s3://${bucket}/data/views/edh_clinical_crmprd

emrfs sync s3://${bucket}/data/views/edh_clinical_intprd


echo $rootdir
echo "Starting the ingestion for testing data"

#sh -x ./sqoop_build.sh -c connection.cfg -f ${alltable} -o sqoop.opt -r ${target} -d ${db} -S ${stagingdb} -T ${targetdb} -V ${view} -P ${secrets} -B ${bucket} -x

sh -x ./sqoop_build.sh -c connection.cfg -f ${alltable} -o sqoop.opt -S ${stagingdb} -T ${targetdb} -V ${view} -P ${secrets} -B ${bucket} -x

status=$?

  if [ "$status" != "0" ]; then
        echo "FAILURE: ingestion for testing data, check the logs for more details"		
  fi
exit $status