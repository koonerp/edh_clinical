import requests
import boto3
import botocore
from botocore.client import Config
import json
import xml.etree.ElementTree as ET
import pyarrow.parquet as pq
import s3fs
import pandas as pd
from datetime import datetime
import time
import os
# from sas7bdat import SAS7BDAT
from pandas.io.sas.sas7bdat import SAS7BDATReader

# Get secret key from Secrets Manager
def get_secret(p_secret_id, p_secret_key):
    currentSession = boto3.session.Session()
    currentRegion = currentSession.region_name
    sm_client = boto3.client('secretsmanager', currentRegion)
    get_secret_value_response = sm_client.get_secret_value(SecretId=p_secret_id)
    if 'SecretString' in get_secret_value_response:
        secret_string = json.loads(get_secret_value_response['SecretString'])
        secret_value = secret_string[p_secret_key]
        return secret_value
    else:
        print('SecretString not found !!!')


# Call Rave Service
def call_rave_service(p_uri, p_rave_username, p_rave_user_pass):
    maxRetry = 5
    response = None

    while maxRetry != 0:

        try:
            response = requests.get(p_uri, auth=(p_rave_username, p_rave_user_pass))
            maxRetry=maxRetry-1
        except (requests.exceptions.ConnectionError, requests.exceptions.ChunkedEncodingError, ConnectionResetError) as error:
        # if response.status_code == 104:
            print('Connection issue, continue with retry...')
            time.sleep(0.01)
            # raise error
            continue
            
        else:    
            return response
    else:
        print('Reached MAX Retry....')
        return response


# Call Rave Service
def call_rave_service_stream(p_uri, p_rave_username, p_rave_user_pass):
    # time.sleep(0.01)
    # response = requests.get(p_uri, auth=(p_rave_username, p_rave_user_pass), stream=True)
    # return response
    maxRetry = 3
    response = None

    while maxRetry != 0:
        response = requests.get(p_uri, auth=(p_rave_username, p_rave_user_pass), stream=True)
        maxRetry=maxRetry-1

        if response.status_code == 104:
            print(response.reason, "; ", 'Continue with retry...')
            time.sleep(0.01)
            continue

        return response
    else:
        print('Reached MAX Retry....')
        return response


# Write data to local file
def write_to_local_file(p_local_filename, p_file_data):
    with open(p_local_filename, 'wb') as f:
        f.write(p_file_data)


# Write data to local file
def write_to_local_file_chunk(p_local_filename, p_response):
    with open(p_local_filename, 'wb') as f:
        for chunk in p_response.iter_content(chunk_size=1024*1024*50):
            if chunk:
                f.write(chunk)
                f.flush()


def upload_to_s3(p_filename, p_bucket_name, p_bucket_dir, p_obj_key, p_kms_key_id):
    s3 = boto3.client('s3', config=Config(signature_version='s3v4'))
    params = {"ServerSideEncryption": "aws:kms", "SSEKMSKeyId": p_kms_key_id}
    s3.upload_file(p_filename, p_bucket_name, p_bucket_dir + p_obj_key, ExtraArgs=params)


def get_dynamodb_item(table_name, service_name):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(table_name)
    resp = table.get_item(Key={'service_name': service_name})
    return resp['Item']


def get_study_oids(file_name):
    tree = ET.parse(file_name)
    root = tree.getroot()
    study_oids = [child.attrib['OID'] for child in root]
    return study_oids


def get_study_oids_prod_suffix(file_name):
    tree = ET.parse(file_name)
    root = tree.getroot()
    study_oids = [child.attrib['OID'] for child in root if '(Prod)' in child.attrib['OID']]
    return study_oids


def get_s3_obj(s3_bucket, s3_obj, file_name):
    s3 = boto3.client('s3')
    print('s3_bucket : ', s3_bucket)
    print('s3_obj : ', s3_obj)
    print('file_name : ', file_name)
    s3.download_fileobj(s3_bucket, s3_obj, file_name)

def sendNotification(sns_topic_name, subject, message):
    sts = boto3.client("sts")
    user_account = sts.get_caller_identity()["Account"]
    sns = boto3.client('sns')
    currentSession = boto3.session.Session()
    currentRegion = currentSession.region_name

    print('sns topic arn : ', ('arn:aws:sns:' + currentRegion + ':' + user_account + ':' + sns_topic_name))
    response = sns.publish(
        TopicArn='arn:aws:sns:' + currentRegion + ':' + user_account + ':' + sns_topic_name,
        Subject=subject,
        Message=message,
        )

#Read CSV file from S3 and return Pandas DataFrame
def get_DataFrame_From_CSV(s3_bucket, inputFileKey):
    s3 = boto3.resource('s3')
    inputFileKey=inputFileKey.strip()
    inputFile = s3.Object(s3_bucket, inputFileKey)
    fileExist = list(s3.Bucket(s3_bucket).objects.filter(Prefix=inputFileKey))
    
    try:
        if(len(fileExist)>0):
            print(f'[{datetime.now()}] File Name from reader : [{inputFileKey}]')
            cols = inputFile.get()['Body'].read().decode('utf-8').splitlines(True)[0]
            columnDict = {}
            columnDict = dict.fromkeys(cols.split(','), str)
            inputFileDF = pd.read_csv(inputFile.get()['Body'], dtype=columnDict)
            # inputFileDF = pd.read_csv(inputFile.get()['Body'])
            removeEOFRecord = inputFileDF['userid'] != 'EOF'
            inputFileDF = inputFileDF[removeEOFRecord]
            return 'SUCCESS', inputFileDF
        else:
            print('[Error] - FILE-NOT-FOUND : ', inputFileKey)
            return 'FILE-NOT-FOUND', inputFileKey
    except (pd.errors.ParserError, pd.errors.EmptyDataError) as parserError:
        print(parserError)
        return 'FORMAT-ERROR', inputFileKey

#Read Parquet file from S3 and return Pandas DataFrame
def getDataFrame_From_Parquet(s3_bucket, inputLocation):
    s3 = boto3.resource('s3')
    fileExist = list(s3.Bucket(s3_bucket).objects.filter(Prefix=(inputLocation[1:])))
    
    if(len(fileExist)==0):
        print('[Error] - FILE-NOT-FOUND : ', inputLocation)
        return 'FILE-NOT-FOUND', inputLocation

    fs = s3fs.S3FileSystem()
    bucket_uri = f's3://{s3_bucket}{inputLocation}'    
    dataset = pq.ParquetDataset(bucket_uri, filesystem=fs)

    table = dataset.read()
    inputFileDF = table.to_pandas()
    
    return len(inputFileDF), inputFileDF

#Convert number to DateTimestamp
def convertBigIntToDateTime(bigIntData):
    date = None
    if bigIntData != None and bigIntData >= 0:
        date = datetime.fromtimestamp(bigIntData / 1e3)

    return date

# Get current milli seconds
# current_milli_time = lambda: str(round(time.time() * 1000))

#Read SAS7BDAT file from S3 and return Pandas DataFrame
def get_DataFrame_From_SAS(s3_bucket, inputFileKey):
    s3 = boto3.resource('s3')
    inputFileKey=inputFileKey.strip()
    inputFile = s3.Object(s3_bucket, inputFileKey)
    fileExist = list(s3.Bucket(s3_bucket).objects.filter(Prefix=inputFileKey))

    if(len(fileExist) == 0):
        print('FILE-NOT-FOUND', inputFileKey, 'is missing, please take action!')
        return 'FILE-NOT-FOUND', inputFileKey, 'is missing, please take action!'

    try:    
        reader = SAS7BDATReader('s3://' + s3_bucket + '/' + inputFileKey, chunksize = 50000, encoding='ISO-8859-2')
        # print(type(reader))
        print('No.of Rows : ', reader.row_count)
        print('No.of Columns : ', reader.column_count)

        inputFileDFs = []
        for chunk in reader:
            inputFileDFs.append(chunk)

        inputFileDF = pd.concat(inputFileDFs)
        inputFileDF = inputFileDF.replace('<NA>', "")
        inputFileDF = inputFileDF.replace('nan', "")

        print('No.of rows : ', len(inputFileDF), ' No.of Columns : ', len(inputFileDF.columns))
        return len(inputFileDF), inputFileDF
    except Exception as error:
        print('Error File : ', inputFileKey)
        print(error)
        return "FORMAT-ERROR", inputFileKey
        
#Read SAS7BDAT file from S3 and return Pandas DataFrame
# def get_DataFrame_From_SAS_Local(s3_bucket, inputFileKey):
#     s3 = boto3.resource('s3')
#     inputFileKey=inputFileKey.strip()
#     inputFile = s3.Object(s3_bucket, inputFileKey)
#     fileExist = list(s3.Bucket(s3_bucket).objects.filter(Prefix=inputFileKey))

#     if(len(fileExist) == 0):
#         print('FILE-NOT-FOUND', inputFileKey, 'is missing, please take action!')
#         return 'FILE-NOT-FOUND', inputFileKey, 'is missing, please take action!'

#     formoid_local_dir = os.path.join(os.getcwd(), os.path.split(os.path.abspath(inputFileKey))[0])

#     try:
#         if not os.path.exists(formoid_local_dir):
#             os.makedirs(formoid_local_dir)
#     except FileExistsError as error:
#         pass
    
#     formoid_local_file = os.path.join(os.getcwd(), inputFileKey) + current_milli_time()

#     try:
#         with open(formoid_local_file, 'wb') as input_data:
#             get_s3_obj(s3_bucket, inputFileKey, input_data)
#             print(formoid_local_file, ' got downloaded from s3')
#     except botocore.exceptions.ClientError as e:
#         if e.response['Error']['Code'] == "404":
#             # TODO: Need to log this in a table
#             print('[ERROR]', inputFileKey, ' is missing, please take action!')
#             return '[ERROR]', inputFileKey, ' is missing, please take action!'
#         elif e.response['Error']['Code'] == "403":
#             print('[ERROR]', inputFileKey, ' is not accessible, please take action!')
#             return '[ERROR]', inputFileKey, ' is not accessible, please take action!'
#         else:
#             print('[ERROR]', inputFileKey, ' is having an issue, please take action!')
#             return '[ERROR]', inputFileKey, ' is having an issue, please take action!'

#     try:
#         with SAS7BDAT(formoid_local_file) as reader:
#             inputFileDF = reader.to_data_frame()
#             inputFileDF = inputFileDF.applymap(str)
#             inputFileDF = inputFileDF.replace('nan', "")

#             return 'SUCCESS', inputFileDF
#     except Exception as error:
#         print('Error File : ', inputFileKey)
#         print(error)
#         return "FORMAT-ERROR", inputFileKey