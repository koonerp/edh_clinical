import json
import os
import subprocess
import boto3


def sendNotification(sns_topic_name, subject, message):
    sts = boto3.client("sts")
    user_account = sts.get_caller_identity()["Account"]
    sns = boto3.client('sns')
    currentSession = boto3.session.Session()
    currentRegion = currentSession.region_name

    print('sns topic arn : ', ('arn:aws:sns:' + currentRegion + ':' + user_account + ':' + sns_topic_name))
    response = sns.publish(
        TopicArn='arn:aws:sns:' + currentRegion + ':' + user_account + ':' + sns_topic_name,
        Subject=subject,
        Message=message,
        )

optFile = 'sqoop.opt'
tableFile = 'tables.txt'
configurationFile = 'connection.cfg'
environment = 'dev'

ssm = boto3.client('ssm')
parameter = ssm.get_parameter(Name='edh_platform_environment')
environment = parameter['Parameter']['Value']

dataLoaderConfig="".join(['./data_loader_conf_', environment ,'.json'])
with open(dataLoaderConfig) as f:
    data = json.load(f)

    for value in data:
        # print(data[value])
        # print(data[value].get('sqoop_options'))
        # dbName = data[value].get('db')
        # target = data[value].get('target')
        stagingdb = data[value].get('stagingdb')
        targetdb = data[value].get('targetdb')
        view = data[value].get('view')
        secrets = data[value].get('secrets')
        s3bucket = data[value].get('s3bucket')
        connection_string = data[value].get('connection_string')
        sqoop_options = data[value].get('sqoop_options')

        with open(configurationFile, 'w', encoding='utf-8') as output_file:
            output_file.write(('connection=' + connection_string))

        with open(optFile, 'w', encoding='utf-8') as output_file:
            output_file.write(data[value].get('sqoop_options'))

        with open(tableFile, 'w', encoding='utf-8') as output_file:
            output_file.writelines("%s\n" % table.strip() for table in data[value].get('tables_to_load').split(','))

        # commandlist = ['sh -x callbuild_external.sh', dbName, environment, "tables.txt",  target, stagingdb, targetdb, view, secrets, s3bucket]        
        commandlist = ['sh -x callbuild_external.sh', environment, "tables.txt",  stagingdb, targetdb, view, secrets, s3bucket]        
        commandStr = " "
        commandStr = commandStr.join(commandlist)

        print('callbuild command : ', commandStr)

        p = subprocess.Popen(commandStr, stdout=subprocess.PIPE, shell=True)

        (output, err) = p.communicate()
        p_status = p.wait()
        print("Command Output : ",  output.decode())
        print('Command exit status : ', p_status)

        if p_status != 0:
            sendNotification('edh_clinical_sns_app_users','Clinical RM One Time data load ' + value + ' in ' + environment,'Hello,\r\n\r\nThe one time data load for ' + value + ' database got failed.\r\n\r\n')
