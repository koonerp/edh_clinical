import sys
import time
import os
from datetime import datetime
from awsglue.utils import getResolvedOptions
from clinical_analytics import get_dynamodb_item, get_secret, call_rave_service, get_study_oids_prod_suffix, \
    write_to_local_file, upload_to_s3, sendNotification
import concurrent.futures
import xml.etree.ElementTree as ET


# ====================================================================
# Script Begins
# ====================================================================
t1 = time.perf_counter()
print(f'[{datetime.now()}] Module Execution Started...')

args = getResolvedOptions(sys.argv, ['dynamodb_table', 'service_name'])
missingArchitectMetadata = []
dynamodb_table_name = args['dynamodb_table']
service_name = args['service_name']

print(f'[{datetime.now()}] Parameter: DynamoDB Table Name [{dynamodb_table_name}]')
print(f'[{datetime.now()}] Parameter: Service Name [{service_name}]')

architect_conf = get_dynamodb_item(dynamodb_table_name, service_name)
secret_name = architect_conf['secret_id']
kms_key_id = architect_conf['kms_key_id']
bucket_name = architect_conf['s3_bucket']
studylist_service_name = architect_conf['study_list_service_name']
site_service_name = architect_conf['site_service_name']

# Get Rave API user credentials from AWS Secrets Manager
print(f'[{datetime.now()}] Getting Rave user credentials from AWS Secrets Manager for [{secret_name}]')
secret_username = get_secret(secret_name, 'username')
secret_password = get_secret(secret_name, 'password')
print(f'[{datetime.now()}] Secret username [{secret_username}], Password length [{len(secret_password)}]')

# Get Study list
t1_studylist = time.perf_counter()
studylist_conf = get_dynamodb_item(dynamodb_table_name, studylist_service_name)
studylist_obj_prefix = studylist_conf['s3_object_prefix']
studylist_obj_key = studylist_conf['s3_object_key'].replace('$$timestamp$$', datetime.now().strftime("%Y%m%d_%H%M%S.%f"))
studylist_dir_path = os.path.join(os.getcwd(), studylist_obj_prefix)
if not os.path.exists(studylist_dir_path):
    os.makedirs(studylist_dir_path)
studylist_localfile = os.path.join(studylist_dir_path, studylist_obj_key)

studylist_uri = studylist_conf['uri_endpoint'] + studylist_conf['uri_resource']
print(f'[{datetime.now()}] Study List URI [{studylist_uri}]')

downloadFlag = False
retry = 0
while not downloadFlag:
    with call_rave_service(studylist_uri, secret_username, secret_password) as resp:
        if resp.status_code == 404 or '<title>Runtime Error</title>' in resp.content.decode():
            retry += 1
            print(f'[{datetime.now()}] Study is having runtime error/Not Found. Response Code : [{resp.status_code}]. Try Again to download - [{retry}]')
            if retry == 5:
                downloadFlag = True
            continue                    
        elif resp.status_code != 200 or len(resp.content) == 0:
            print(f'Empty File : [{studylist_localfile}] : Resonse Code : [{resp.status_code}] : Content Size : [{len(resp.content)}] : [{(resp.status_code != 200)}]')
            retry += 1
            if retry == 5:
                downloadFlag = True
            continue

        print(f'[{datetime.now()}] Study List local file [{studylist_localfile}]')
        write_to_local_file(studylist_localfile, resp.content)
        print(f'[{datetime.now()}] Study List local file write completed')
        upload_to_s3(studylist_localfile, bucket_name, studylist_obj_prefix, studylist_obj_key, kms_key_id)
        print(f'[{datetime.now()}] Study List uploaded to S3')
        downloadFlag = True

with open(studylist_localfile, 'rb') as f:
    # all_studyoids = get_study_oids(f.name)
    # just in Dev, call above function in PROD
    all_studyoids = get_study_oids_prod_suffix(f.name)
print(f'[{datetime.now()}] All (Prod) suffix Study OIDs [{len(all_studyoids)}] {all_studyoids}')

studyoids = []
if 'exclusion_list' in studylist_conf:
    exclusion_list = list(studylist_conf['exclusion_list'].split(','))
    studyoids = [study for study in all_studyoids if study not in exclusion_list]
else:
    studyoids = all_studyoids
    
print(f'[{datetime.now()}] Study OIDs after exclusion [{len(studyoids)}] {studyoids}')
t2_studylist = round(time.perf_counter() - t1_studylist)
print(f'[{datetime.now()}] Study List processing finished in [{t2_studylist}] seconds')

def download_architect_metadata_data(study_name):
    time.sleep(0.01)
    t1_architect_metadata = time.perf_counter()
    debug_msgs = []
    study_name_for_s3 = study_name.replace(' ', '_')
    study_name_for_uri = study_name.replace(' ', '%20').replace('(Prod)','')
    
    # Get Study Sites
    t1_sites = time.perf_counter()
    sites_conf = get_dynamodb_item(dynamodb_table_name, site_service_name)
    sites_obj_prefix = sites_conf['s3_object_prefix']
    sites_dir_path = os.path.join(os.getcwd(), sites_obj_prefix, study_name_for_s3)
    if not os.path.exists(sites_dir_path):
        os.makedirs(sites_dir_path)
    sites_obj_key = sites_conf['s3_object_key'].replace('$$study_id$$', study_name_for_s3)
    sites_localfile = os.path.join(sites_dir_path, sites_obj_key)
    sites_uri = sites_conf['uri_endpoint']
    sites_uri += sites_conf['uri_resource'].replace('$$study_id$$', study_name_for_uri)
    if 'uri_parameters' in sites_conf:
        sites_uri += sites_conf['uri_parameters'].replace('$$study_id_param$$', study_name)

    debug_msgs.append(f'[{datetime.now()}] [{study_name}] Sites URI [{sites_uri}]')
    print(f'[{datetime.now()}] [{study_name}] Sites URI [{sites_uri}]')

    downloadFlag = False
    retry = 0
    while not downloadFlag:
        with call_rave_service(sites_uri, secret_username, secret_password) as meta_resp:
            if meta_resp.status_code == 404 or '<title>Runtime Error</title>' in meta_resp.content.decode():
                retry += 1
                debug_msgs.append(f'[{datetime.now()}] [{study_name}] is having runtime error/Not Found. Response Code : [{meta_resp.status_code}]. Try Again to download - [{retry}]')
                if retry == 5:
                    downloadFlag = True
                continue                    
            elif meta_resp.status_code != 200 or len(meta_resp.content) == 0:
                debug_msgs.append(f'Empty File : [{sites_localfile}] : Resonse Code : [{meta_resp.status_code}] : Content Size : [{len(meta_resp.content)}] : [{(meta_resp.status_code != 200)}]')
                retry += 1
                if retry == 5:
                    downloadFlag = True
                continue

            # if meta_resp.status_code == 500 or len(meta_resp.content) == 0:
            #     # debug_msgs.append(f'Empty File : [{sites_localfile}] : Resonse Code : [{meta_resp.status_code}] : Content Size : [{len(meta_resp.content)}]')
            #     continue
            debug_msgs.append(f'[{datetime.now()}] [{study_name}] Sites local file [{sites_localfile}]')
            write_to_local_file(sites_localfile, meta_resp.content)
            debug_msgs.append(f'[{datetime.now()}] [{study_name}] Sites local file write completed. File size : [{(os.path.getsize(sites_localfile))}] : Content Size : [{len(meta_resp.content)}] : Response Status : [{meta_resp.status_code}]')
            upload_to_s3(sites_localfile, bucket_name, sites_obj_prefix, sites_obj_key, kms_key_id)
            debug_msgs.append(f'[{datetime.now()}] [{study_name}] Sites uploaded to S3')
            downloadFlag = True
        
    tree = ET.parse(sites_localfile)
    root = tree.getroot()
    
    meta_data_version_oids = [elem.attrib['MetaDataVersionOID'] for elem in root.iter() if 'MetaDataVersionOID' in elem.attrib]
    print('Study Name : ', study_name)
    meta_data_version_oids = list(map(int, meta_data_version_oids))

    if len(meta_data_version_oids) == 0:
        print(sites_obj_prefix + sites_obj_key + " doesn't have MetaDataVersionOID")
        missingArchitectMetadata.append(sites_obj_prefix + sites_obj_key + " doesn't have MetaDataVersionOID")
        return
    else:
        metaDataVersionOID = str(max(meta_data_version_oids))
    
    print('meta_data_version_oids : ', metaDataVersionOID)
    
    debug_msgs.append(f'[{datetime.now()}] [{study_name}] MetaDataVersionOID : {metaDataVersionOID}')
    sites_exec = round(time.perf_counter() - t1_sites)
    debug_msgs.append(f'[{datetime.now()}] [{study_name}] Metadata finished in [{sites_exec}] seconds')
    
    architect_obj_prefix = architect_conf['s3_object_prefix']
    architect_dir_path = os.path.join(os.getcwd(), architect_obj_prefix)
    if not os.path.exists(architect_dir_path):
        os.makedirs(architect_dir_path)
        
    def process_metadataversionoid(metaDataVersionOID):
        architect_obj_key = architect_conf['s3_object_key'].replace('$$study_id$$', study_name_for_s3)
        architect_localfile = os.path.join(architect_dir_path, architect_obj_key)


        architect_uri = architect_conf['uri_endpoint']
        architect_uri += architect_conf['uri_resource'].replace('$$study_id$$', study_name_for_uri)
        architect_uri = architect_uri.replace('$$metadataversionoid$$', metaDataVersionOID)

        debug_msgs.append(f'[{datetime.now()}] [{study_name}] [{metaDataVersionOID}] URI [{architect_uri}]')
        downloadFlag = False
        retry = 0
        while not downloadFlag:
            with call_rave_service(architect_uri, secret_username, secret_password) as metaDataVersionOID_resp:
                if metaDataVersionOID_resp.status_code == 404 or '<title>Runtime Error</title>' in metaDataVersionOID_resp.content.decode():
                    retry += 1
                    debug_msgs.append(f'[{datetime.now()}] [{study_name}] is having runtime error/Not Found. Response Code : [{metaDataVersionOID_resp.status_code}]. Try Again to download - [{retry}]')
                    if retry == 5:
                        downloadFlag = True
                    continue                    
                elif metaDataVersionOID_resp.status_code != 200 or len(metaDataVersionOID_resp.content) == 0:
                    debug_msgs.append(f'Empty File : [{sites_localfile}] : Resonse Code : [{metaDataVersionOID_resp.status_code}] : Content Size : [{len(metaDataVersionOID_resp.content)}] : [{(metaDataVersionOID_resp.status_code != 200)}]')
                    retry += 1
                    if retry == 5:
                        downloadFlag = True
                    continue

                debug_msgs.append(f'[{datetime.now()}] [{study_name}] [{metaDataVersionOID}] local file [{architect_localfile}]')
                write_to_local_file(architect_localfile, metaDataVersionOID_resp.content)
                debug_msgs.append(f'[{datetime.now()}] [{study_name}] [{metaDataVersionOID}] local file write completed')
                upload_to_s3(architect_localfile, bucket_name, architect_obj_prefix, architect_obj_key, kms_key_id)
                debug_msgs.append(f'[{datetime.now()}] [{study_name}] [{metaDataVersionOID}] uploaded to S3')
                downloadFlag = True

        return f'[{datetime.now()}] [{study_name}] [{metaDataVersionOID} processing completed]'

    process_metadataversionoid(metaDataVersionOID)

    t2_architect_metadata = round(time.perf_counter() - t1_architect_metadata)
    debug_msgs.append(f'[{datetime.now()}] [{study_name}] Finished in [{t2_architect_metadata}] seconds')

    return debug_msgs
    
with concurrent.futures.ThreadPoolExecutor(max_workers=50) as executor:
    study_results = {executor.submit(download_architect_metadata_data, study_oid): study_oid for study_oid in studyoids}
    for study_result in concurrent.futures.as_completed(study_results):
        if study_result.result() is not None:
            for study_ret_msg in study_result.result():
                if study_ret_msg is not None:
                    try:
                        print(study_ret_msg)
                    except Exception as ex:
                        print(ex)
                        raise ex


print('No.of missing studies : ', len(missingArchitectMetadata))
if len(missingArchitectMetadata) > 0:
    missingArchitectMetadataStr = '\r\n'.join(missingArchitectMetadata)
    subject = '''Studies doesn't have MetaDataVersionOID - ''' + service_name
    message = 'Hello,\r\n\r\nThe following studies does not have MetaDataVersionOID.\r\n\r\n' + missingArchitectMetadataStr
    
    sendNotification('edh_clinical_sns_app_users', subject, message)

print(f'[{datetime.now()}] Module Execution Completed...')
t2 = round(time.perf_counter() - t1)
print(f'Finished in [{t2}] seconds')
# ====================================================================
# Script Ends
# ====================================================================