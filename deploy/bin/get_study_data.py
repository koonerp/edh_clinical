import time
from datetime import datetime
import sys
import os
from awsglue.utils import getResolvedOptions
import concurrent.futures
from clinical_analytics import get_secret, call_rave_service, write_to_local_file, get_dynamodb_item, \
    get_study_oids_prod_suffix, upload_to_s3


# ==============================================================================
# Script Begins
# ==============================================================================
t1 = time.perf_counter()
print(f'[{datetime.now()}] Module Execution Started...')

# dynamodb_table_name = sys.argv[1]
# service_name = sys.argv[2]

args = getResolvedOptions(sys.argv, ['dynamodb_table', 'service_name'])
dynamodb_table_name = args['dynamodb_table']
service_name = args['service_name']

print(f'[{datetime.now()}] Parameter: DynamoDB Table Name [{dynamodb_table_name}]')
print(f'[{datetime.now()}] Parameter: Service Name [{service_name}]')

# Initialize Variables from DynamoDB Table
studydata_conf = get_dynamodb_item(dynamodb_table_name, service_name)
secret_name = studydata_conf['secret_id']
kms_key_id = studydata_conf['kms_key_id']
bucket_name = studydata_conf['s3_bucket']
studydata_obj_prefix = studydata_conf['s3_object_prefix']
studylist_service_name = studydata_conf['study_list_service_name']

# Get Rave API user credentials from AWS Secrets Manager
print(f'[{datetime.now()}] Getting Rave user credentials from AWS Secrets Manager for [{secret_name}]')
secret_username = get_secret(secret_name, 'username')
secret_password = get_secret(secret_name, 'password')
print(f'[{datetime.now()}] Secret username [{secret_username}], Password length [{len(secret_password)}]')

# Get Study list
t1_studylist = time.perf_counter()
studylist_conf = get_dynamodb_item(dynamodb_table_name, studylist_service_name)
studylist_obj_prefix = studylist_conf['s3_object_prefix']
studylist_obj_key = studylist_conf['s3_object_key'].replace('$$timestamp$$', datetime.now().strftime("%Y%m%d_%H%M%S.%f"))
studylist_dir_path = os.path.join(os.getcwd(), studylist_obj_prefix)
if not os.path.exists(studylist_dir_path):
    os.makedirs(studylist_dir_path)
studylist_localfile = os.path.join(studylist_dir_path, studylist_obj_key)

studylist_uri = studylist_conf['uri_endpoint'] + studylist_conf['uri_resource']
print(f'[{datetime.now()}] Study List URI [{studylist_uri}]')
downloadFlag = False
retry = 0
while not downloadFlag:
    with call_rave_service(studylist_uri, secret_username, secret_password) as resp:
        if resp.status_code == 404 or '<title>Runtime Error</title>' in resp.content.decode():
            retry += 1
            print(f'[{datetime.now()}] Study is having runtime error/Not Found. Response Code : [{resp.status_code}]. Try Again to download - [{retry}]')
            if retry == 5:
                downloadFlag = True
            continue                    
        elif resp.status_code != 200 or len(resp.content) == 0:
            print(f'Empty File : [{studylist_localfile}] : Resonse Code : [{resp.status_code}] : Content Size : [{len(resp.content)}] : [{(resp.status_code != 200)}]')
            retry += 1
            if retry == 5:
                downloadFlag = True
            continue

        print(f'[{datetime.now()}] Study List local file [{studylist_localfile}]')
        write_to_local_file(studylist_localfile, resp.content)
        print(f'[{datetime.now()}] Study List local file write completed')
        upload_to_s3(studylist_localfile, bucket_name, studylist_obj_prefix, studylist_obj_key, kms_key_id)
        print(f'[{datetime.now()}] Study List uploaded to S3')
        downloadFlag = True

with open(studylist_localfile, 'rb') as f:
    # all_studyoids = get_study_oids(f.name)
    # just in Dev, call above function in PROD
    all_studyoids = get_study_oids_prod_suffix(f.name)
print(f'[{datetime.now()}] All (Prod) suffix Study OIDs [{len(all_studyoids)}] {all_studyoids}')

studyoids = []
if 'exclusion_list' in studylist_conf:
    exclusion_list = list(studylist_conf['exclusion_list'].split(','))
    studyoids = [study for study in all_studyoids if study not in exclusion_list]
else:
    studyoids = all_studyoids
    
print(f'[{datetime.now()}] Study OIDs after exclusion [{len(studyoids)}] {studyoids}')
t2_studylist = round(time.perf_counter() - t1_studylist)
print(f'[{datetime.now()}] Study List processing finished in [{t2_studylist}] seconds')


def download_study_data(study_name):
    t1_studydata = time.perf_counter()
    debug_msgs = []
    study_name_for_s3 = study_name.replace(' ', '_')
    study_name_for_uri = study_name.replace(' ', '%20')
    studydata_dir_path = os.path.join(os.getcwd(), studydata_obj_prefix, study_name_for_s3)
    if not os.path.exists(studydata_dir_path):
        os.makedirs(studydata_dir_path)
    studydata_obj_key = studydata_conf['s3_object_key'].replace('$$study_id$$', study_name_for_s3)
    studydata_localfile = os.path.join(studydata_dir_path, studydata_obj_key)
    uri = studydata_conf['uri_endpoint']
    uri += studydata_conf['uri_resource'].replace('$$study_id$$', study_name_for_uri)
    if 'uri_parameters' in studydata_conf:
        uri += studydata_conf['uri_parameters'].replace('$$study_id_param$$', study_name)
    debug_msgs.append(f'[{datetime.now()}] [{study_name}] URI [{uri}]')

    downloadFlag = False
    retry = 0
    while not downloadFlag:
        with call_rave_service(uri, secret_username, secret_password) as meta_resp:
            if meta_resp.status_code == 404 or '<title>Runtime Error</title>' in meta_resp.content.decode():
                retry += 1
                debug_msgs.append(f'[{datetime.now()}] [{study_name}] is having runtime error/Not Found. Response Code : [{meta_resp.status_code}]. Try Again to download - [{retry}]')
                if retry == 5:
                    downloadFlag = True
                continue                    
            elif meta_resp.status_code != 200 or len(meta_resp.content) == 0:
                debug_msgs.append(f'Empty File : [{studydata_localfile}] : Resonse Code : [{meta_resp.status_code}] : Content Size : [{len(meta_resp.content)}] : [{(meta_resp.status_code != 200)}]')
                retry += 1
                if retry == 5:
                    downloadFlag = True
                continue
            
            debug_msgs.append(f'[{datetime.now()}] [{study_name}] Studydata local file [{studydata_localfile}]')
            write_to_local_file(studydata_localfile, meta_resp.content)
            debug_msgs.append(f'[{datetime.now()}] [{study_name}] Studydata local file write completed')
            upload_to_s3(studydata_localfile, bucket_name, studydata_obj_prefix, studydata_obj_key, kms_key_id)
            debug_msgs.append(f'[{datetime.now()}] [{study_name}] Uploaded to S3')
            downloadFlag = True
            
    t2_studydata = round(time.perf_counter() - t1_studydata)
    debug_msgs.append(f'[{datetime.now()}] [{study_name}] Finished in [{t2_studydata}] seconds')

    return debug_msgs


with concurrent.futures.ThreadPoolExecutor(max_workers=50) as executor:
    studydata_results = {executor.submit(download_study_data, study_oid): study_oid for study_oid in studyoids}
    for study_result in concurrent.futures.as_completed(studydata_results):
        for studydata_ret_str in study_result.result():
            try:
                print(studydata_ret_str)
            except Exception as ex:
                print(ex)
                raise ex


print(f'[{datetime.now()}] Module Execution Completed...')
t2 = round(time.perf_counter() - t1)
print(f'Finished in [{t2}] seconds')
# ====================================================================
# Script Ends
# ====================================================================
