#!/bin/bash

s3bucket=$1

environment=$2

applicationBucket="s3://"${s3bucket}

aws s3 cp ${applicationBucket}/deploy/bin/sqoop_data_loader.py .

aws s3 cp ${applicationBucket}/deploy/bin/callbuild_external.sh .

aws s3 cp ${applicationBucket}/deploy/resource/data_loader_conf_${environment}.json .

sudo pip-3.6 install boto3

python3 sqoop_data_loader.py
